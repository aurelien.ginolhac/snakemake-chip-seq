`Quality score distribution plot
<https://gatk.broadinstitute.org/hc/en-us/articles/360037057312-QualityScoreDistribution-Picard->`_ **(Picard)** is used
as overall quality control for a library in a given run. It shows for the range of quality scores the corresponding
total numbers of bases. For more information about `collected Picard metrics
<https://gatk.broadinstitute.org/hc/en-us/articles/360037594031-CollectMultipleMetrics-Picard->`_ please
see `documentation <https://broadinstitute.github.io/picard/>`_.

