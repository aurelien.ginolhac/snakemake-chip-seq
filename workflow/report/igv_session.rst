The IGV session can be downloaded here. It contains all files necessary for the session.
Unzip the directory 'report_igv_session.zip'.
`Install IGV <http://software.broadinstitute.org/software/igv/download>`_ and open session by selecting
'report_igv_session.xml'.

Alternatively you can also start the IGV session directly from the results of the workflow.
The session file is located in 'results/IGV/igv_session.xml'.
