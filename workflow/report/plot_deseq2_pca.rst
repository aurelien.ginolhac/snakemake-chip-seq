`PCA plot <https://bioconductor.org/packages/release/bioc/manuals/DESeq2/man/DESeq2.pdf#Rfn.plotPCA>`_
**(Principal Component Analysis)** describes variance of the
`rlog <https://bioconductor.org/packages/release/bioc/manuals/DESeq2/man/DESeq2.pdf#Rfn.rlog>`_ or
`vst <https://bioconductor.org/packages/release/bioc/manuals/DESeq2/man/DESeq2.pdf#Rfn.vst>`_ transformed counts from
DESeq2 analysis with regard to the used {{snakemake.wildcards["antibody"]}} antibody. For more information about DESeq2 please see
`documentation <https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html>`_.
