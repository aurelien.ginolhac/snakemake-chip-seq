rule fastqc:
    input:
        get_individual_fastq
    output:
        html="results/qc/fastqc/{sample}.{unit}.{read}.html",
        zip="results/qc/fastqc/{sample}.{unit}.{read}_fastqc.zip"
    params:
        ""
    log:
        "logs/fastqc/{sample}.{unit}.{read}.log"
    threads: 4
    wrapper:
        "0.72.0/bio/fastqc"

rule multiqc:
    input:
        get_multiqc_input
    output:
        report("results/qc/multiqc/multiqc.html", caption="../report/multiqc_report.rst", category="MultiQC")
    log:
        "logs/multiqc.log"
    wrapper:
        "0.64.0/bio/multiqc"

rule fastq_screen:
    input:
        get_individual_fastq
    output:
        txt="results/qc/fastq_screen/{sample}.{unit}.{read}.fastq_screen.txt",
        png="results/qc/fastq_screen/{sample}.{unit}.{read}.fastq_screen.png"
    log:
        "logs/fastq_screen/{sample}.{unit}.{read}.log"
    params:
        fastq_screen_config = {
            'database': {
                'human': {
                  'bowtie2': "{}/Human/Homo_sapiens.GRCh38".format(config["params"]["db_bowtie_path"])},
                'mouse': {
                    'bowtie2': "{}/Mouse/Mus_musculus.GRCm38".format(config["params"]["db_bowtie_path"])},
                'rat':{
                  'bowtie2': "{}/Rat/Rnor_6.0".format(config["params"]["db_bowtie_path"])},
                'drosophila':{
                  'bowtie2': "{}/Drosophila/BDGP6".format(config["params"]["db_bowtie_path"])},
                'worm':{
                  'bowtie2': "{}/Worm/Caenorhabditis_elegans.WBcel235".format(config["params"]["db_bowtie_path"])},
                'yeast':{
                  'bowtie2': "{}/Yeast/Saccharomyces_cerevisiae.R64-1-1".format(config["params"]["db_bowtie_path"])},
                'arabidopsis':{
                  'bowtie2': "{}/Arabidopsis/Arabidopsis_thaliana.TAIR10".format(config["params"]["db_bowtie_path"])},
                'ecoli':{
                  'bowtie2': "{}/E_coli/Ecoli".format(config["params"]["db_bowtie_path"])},
                'rRNA':{
                  'bowtie2': "{}/rRNA/GRCm38_rRNA".format(config["params"]["db_bowtie_path"])},
                'MT':{
                  'bowtie2': "{}/Mitochondria/mitochondria".format(config["params"]["db_bowtie_path"])},
                'PhiX':{
                  'bowtie2': "{}/PhiX/phi_plus_SNPs".format(config["params"]["db_bowtie_path"])},
                'Lambda':{
                  'bowtie2': "{}/Lambda/Lambda".format(config["params"]["db_bowtie_path"])},
                'vectors':{
                  'bowtie2': "{}/Vectors/Vectors".format(config["params"]["db_bowtie_path"])},
                'adapters':{
                  'bowtie2': "{}/Adapters/Contaminants".format(config["params"]["db_bowtie_path"])},
                'mycoplasma':{
                  'bowtie2': "{}/Mycoplasma/mycoplasma".format(config["params"]["db_bowtie_path"])}
                 },
                 'aligner_paths': {'bowtie2': "{}/bowtie2".format(config["params"]["bowtie_path"])}
                },
        subset=100000,
        aligner='bowtie2'
    threads: 6
    wrapper:
        "0.65.0/bio/fastq_screen"
