**MACS2 and bedtools** merged consensus {{ snakemake.wildcards.peak }} peaks plot is generated by calculating the
proportion of intersection size assigned to all samples for {{ snakemake.wildcards.antibody }}.
