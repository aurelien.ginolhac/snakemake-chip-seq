**deepTools** `plotHeatmap <https://deeptools.readthedocs.io/en/develop/content/tools/plotHeatmap.html>`_ creates a
heatmap of read coverage over sets of genomic regions and gives a visualisation for the genome-wide enrichment of the samples.
