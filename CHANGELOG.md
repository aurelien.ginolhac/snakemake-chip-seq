
## v0.1.1

- adaptative peak calling according to the histone mark

## v0.1.0

- added a changelog

## v0.0.9


### Main changes from the snakemake worlflow template

- [Singularity](https://sylabs.io/singularity/) using a docker image published on [Docker hub](https://hub.docker.com/r/ginolhac/snake-chip-seq)
- [AdapterRemoval](https://adapterremoval.readthedocs.io/en/latest/) for trimming (replacement of `cutadapt`)
- add [FastqScreen](https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/)
- drop `DESeq2` for contrast in favor to the dedicated tool: [`DiffBind`](https://bioconductor.org/packages/release/bioc/html/DiffBind.html) #TODO
- don't import the IGV session in report, the HTML becomes too large