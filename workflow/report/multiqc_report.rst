The **MultiQC** report is a collection of multiple plots, stats and metrics from phantompeakqualtools (Chip-seq processing), Preseq, Picard,  Samtools and FastQC.
For detailed descriptions of the individual plots and statistics, load the MultiQC report by clicking on it.
