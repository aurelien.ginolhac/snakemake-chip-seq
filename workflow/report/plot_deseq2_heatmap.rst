`Heatmap plot <https://cran.r-project.org/web/packages/pheatmap/pheatmap.pdf>`_ shows a heatmap of the `rlog <https://bioconductor.org/packages/release/bioc/manuals/DESeq2/man/DESeq2.pdf#Rfn.rlog>`_ or
`vst <https://bioconductor.org/packages/release/bioc/manuals/DESeq2/man/DESeq2.pdf#Rfn.vst>`_ transformed counts from
DESeq2 analysis for all groups treated with the {{snakemake.wildcards["antibody"]}} antibody
. For more information about DESeq2 please see
`documentation <https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html>`_.

