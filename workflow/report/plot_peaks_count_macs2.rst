**MACS2 peak count** is calculated from total number of peaks called by
`MACS2 <https://github.com/taoliu/MACS>`_.
