**ChIP-seq** peak-calling, QC and differential analysis pipeline https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-chip-seq,
 derived from the Snakemake template https://github.com/snakemake-workflows/chipseq
