**deepTools** `plotProfile <https://deeptools.readthedocs.io/en/develop/content/tools/plotProfile.html>`_ creates a
profile plot of read coverage over sets of genomic regions and gives a visualisation for the genome-wide enrichment of the
samples.
