`Homer annotatePeaks <http://homer.ucsd.edu/homer/ngs/annotation.html>`_ assigns known genomic features to peaks.
For each sample-control pair, plots show the peak locations relative to annotated features, the percentage of unique genes to closest peak and the peak distribution relative to TSS (Transcription Start Site).
