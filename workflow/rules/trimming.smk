# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule trimming_pe:
    input:
        # unpack to convert the returned dict to a list
        unpack(get_fastq)
    output:
        fq1="results/trimmed_pe/{sample}-{unit}.1.fastq.gz",
        fq2="results/trimmed_pe/{sample}-{unit}.2.fastq.gz",
        single="results/trimmed_pe/{sample}-{unit}.singletons.gz",
        discarded="results/trimmed_pe/{sample}-{unit}.discarded.gz",
        settings="results/trimmed_pe/{sample}-{unit}.settings"
    params:
        adapter = config["trimming"]["pe"],
        others = config["trimming"]["others"]
    threads: config["trimming"]["threads"]
    log:
        "logs/trimming/{sample}-{unit}.pe.log"
    shell:
        "AdapterRemoval --file1 {input.fq1} --file2 {input.fq2} "
        "--output1 {output.fq1} --output2 {output.fq2} "
        "--singleton {output.single} "
        "--settings {output.settings} --discarded {output.discarded} "
        " {params.adapter} "
        "--threads {threads} {params.others}"
# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule trimming_se:
    input:
      unpack(get_fastq)
    output:
        fastq="results/trimmed_se/{sample}-{unit}.fastq.gz",
        settings="results/trimmed_se/{sample}-{unit}.settings",
        discarded="results/trimmed_se/{sample}-{unit}.discarded.gz"
    params:
        adapter = config["trimming"]["se"],
        others = config["trimming"]["others"]
    threads: config["trimming"]["threads"]
    log:
        "logs/trimming/{sample}-{unit}.se.log"
    shell:
        "AdapterRemoval --file1 {input.fq1} "
        "--output1 {output.fastq} "
        "--settings {output.settings} --discarded {output.discarded} "
        " {params.adapter} "
        "--threads {threads} {params.others}"
