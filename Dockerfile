FROM rocker/r-ubuntu:20.04

LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"

# build with: docker build -t ginolhac/snake-chip-seq:0.1 .
# docker push ginolhac/snake-chip-seq:0.1

LABEL version="0.1"
LABEL description="Docker image to build the snakemake chip-seq singularity"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:openjdk-r/ppa && \
    apt-get update && apt-get install -y \
        build-essential \
        cmake \
        curl \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl \
        gawk \
        openjdk-8-jdk \
        python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

# Use gawk instead of awk (needed for phantompeakqualtools)
RUN update-alternatives --set awk /usr/bin/gawk

# default to python3
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.8-config && ln -sf /usr/bin/python3 /usr/bin/python

# htslib
ARG HTS_VERSION="1.12"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/htslib/releases/download/${HTS_VERSION}/htslib-${HTS_VERSION}.tar.bz2 && \
    tar -xf htslib-${HTS_VERSION}.tar.bz2 && rm htslib-${HTS_VERSION}.tar.bz2 && cd htslib-${HTS_VERSION} && \
    ./configure --enable-libcurl --enable-s3 --enable-plugins --enable-gcs && \
    make && make install && make clean

# samtools
ARG SAM_VERSION="1.12"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/samtools/releases/download/${SAM_VERSION}/samtools-${SAM_VERSION}.tar.bz2 && \
    tar -xf samtools-${SAM_VERSION}.tar.bz2 && rm samtools-${SAM_VERSION}.tar.bz2 && cd samtools-${SAM_VERSION} && \
    ./configure --with-htslib=/opt/htslib-${HTS_VERSION} && make && make install && make clean

# Picard tools
ARG PIC_VERSION="2.25.6"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/broadinstitute/picard/releases/download/${PIC_VERSION}/picard.jar
COPY workflow/scripts/picard /usr/local/bin/picard


# AdapterRemoval
ARG AR_VERSION="2.3.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/MikkelSchubert/adapterremoval/archive/v${AR_VERSION}.tar.gz && \
    tar -xf v${AR_VERSION}.tar.gz && rm v${AR_VERSION}.tar.gz && \
    cd adapterremoval-${AR_VERSION} && make && make install && make clean

RUN mkdir /opt/ucsc && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bigWigToBedGraph && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig && \
    chmod 755 /opt/ucsc/*
ENV PATH /opt/ucsc:$PATH

# Preseq
ARG PRESEQ_VERSION="3.1.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/smithlabcode/preseq/releases/download/v${PRESEQ_VERSION}/preseq-${PRESEQ_VERSION}.tar.gz && \
    tar -xf preseq-${PRESEQ_VERSION}.tar.gz && rm preseq-${PRESEQ_VERSION}.tar.gz && \
    cd preseq-${PRESEQ_VERSION} && ./configure --prefix=/usr/local/ && make && make install && make clean

# HOMER
RUN mkdir /opt/HOMER && cd /opt/HOMER && \
    wget --no-check-certificate http://homer.ucsd.edu/homer/configureHomer.pl && \
    perl configureHomer.pl -install
ENV PATH /opt/HOMER/bin:$PATH

# BAMTOOLS
ARG BAM_VERSION="2.5.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/pezmaster31/bamtools/archive/refs/tags/v${BAM_VERSION}.tar.gz && \
    tar -xf v${BAM_VERSION}.tar.gz && rm v${BAM_VERSION}.tar.gz && \
    cd bamtools-${BAM_VERSION} && cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ && make && make install && make clean

# BWA
ARG BWA_VERSION="0.7.17"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/lh3/bwa/releases/download/v${BWA_VERSION}/bwa-${BWA_VERSION}.tar.bz2 && \
    tar -xf bwa-${BWA_VERSION}.tar.bz2 && rm bwa-${BWA_VERSION}.tar.bz2 && \
    cd bwa-${BWA_VERSION} && make && mv bwa /usr/local/bin/ && make clean

# bedtools
ARG BED_VERSION="2.30.0"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/arq5x/bedtools2/releases/download/v${BED_VERSION}/bedtools-${BED_VERSION}.tar.gz && \
    tar -xf bedtools-${BED_VERSION}.tar.gz && rm bedtools-${BED_VERSION}.tar.gz && \
    cd bedtools2 && make && mv bin/* /usr/local/bin/ && make clean

# FastqScreen
ENV PERL_MM_USE_DEFAULT=1
RUN cd /opt && \
    wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/fastq_screen_v0.14.0.tar.gz && \
    tar -xf fastq_screen_v0.14.0.tar.gz && rm fastq_screen_v0.14.0.tar.gz && perl -MCPAN -e 'install GD::Graph'
ENV PATH /opt/fastq_screen_v0.14.0:$PATH

# fastqc
RUN cd /opt && \
    wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip && \
    unzip fastqc_v0.11.9.zip && rm fastqc_v0.11.9.zip && chmod +x FastQC/fastqc
ENV PATH /opt/FastQC:$PATH

# IGV
RUN cd /opt && wget --no-check-certificate http://data.broadinstitute.org/igv/projects/downloads/2.4/IGV_2.4.14.zip && \
    unzip IGV_2.4.14.zip && cd IGV_2.4.14 && sed -i 's/Xmx4000/Xmx8000/g' igv.sh && \
    ln -s /opt/IGV_2.4.14/igv.sh /usr/local/bin/igv

# Subread
ARG SR_VERSION="2.0.2"
RUN cd /opt && \
    wget --no-check-certificate -O subread.tgz https://sourceforge.net/projects/subread/files/subread-${SR_VERSION}/subread-${SR_VERSION}-Linux-x86_64.tar.gz/download && \
    tar -xf subread.tgz && rm subread.tgz
ENV PATH /opt/subread-${SR_VERSION}-Linux-x86_64/bin:$PATH

# python modules
RUN python3 -m pip install snakemake multiqc pandas pyBigWig pysam macs2 deeptools

# R is 4.0.0
# BiocManager 1.30.16, for BioC 3.12, #FIXME SPP is 1.16.0 if fine, avoid installing 1.15.2 line 148
RUN install2.r --error littler BiocManager && \
    /usr/local/lib/R/site-library/littler/examples/installBioc.r DESeq2 apeglm pheatmap vsn Rsamtools && \
    install2.r --error hexbin ggrepel cowplot tidyverse UpSetR
# DESeq2 is 1.30.1
# apeglm 1.12.0

# Install phantompeakqualtools, SPP fixed version 1.15.2
ENV PHANTOMPEAKQUALTOOLS_VERSION="1.2.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/kundajelab/phantompeakqualtools/archive/refs/tags/${PHANTOMPEAKQUALTOOLS_VERSION}.tar.gz && \
    tar -xf ${PHANTOMPEAKQUALTOOLS_VERSION}.tar.gz && rm ${PHANTOMPEAKQUALTOOLS_VERSION}.tar.gz && \
    cd phantompeakqualtools-${PHANTOMPEAKQUALTOOLS_VERSION} && \
    wget https://github.com/hms-dbmi/spp/archive/1.15.2.tar.gz && \
    install2.r --error caTools optparse reshape2 && \
    Rscript -e 'install.packages("1.15.2.tar.gz")' && ln -s /opt/phantompeakqualtools-1.2.2/run_spp.R /usr/local/bin/

# Bowtie2
ARG BT2_VERSION="2.4.4"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/BenLangmead/bowtie2/releases/download/v${BT2_VERSION}/bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    unzip bowtie2-${BT2_VERSION}-linux-x86_64.zip && rm bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    mv bowtie2-${BT2_VERSION}-linux-x86_64/bowtie2* /usr/local/bin/ && rm -rf bowtie2-${BT2_VERSION}-linux-x86_64

# clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    apt-get autoclean && \
    apt-get autoremove -y && rm -rf /var/lib/{dpkg,cache,log}/ # keep /var/lib/apt


