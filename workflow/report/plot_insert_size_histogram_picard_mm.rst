`Insert size histogram
<https://gatk.broadinstitute.org/hc/en-us/articles/360037055772-CollectInsertSizeMetrics-Picard->`_ **(Picard)** is used
for validating paired-end library construction. It shows the insert size distribution versus fractions of read pairs in
each of the three orientations (FR, RF, and TANDEM) as a histogram. For more information about `collected Picard metrics
<https://gatk.broadinstitute.org/hc/en-us/articles/360037594031-CollectMultipleMetrics-Picard->`_ please
see `documentation <https://broadinstitute.github.io/picard/>`_.
