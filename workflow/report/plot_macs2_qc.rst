`MACS2 <https://github.com/macs3-project/MACS/blob/master/README.md>`_ **callpeak quality control plots** show for all
samples and their associated controls the number of peaks and their distributions of peak length, the
`fold-change <https://github.com/macs3-project/MACS/blob/master/docs/callpeak.md#output-files>`_ of
the enrichment for their peak summit against random Poisson distribution, FDR and p-value from the results of the
`MACS callpeak analysis <https://hbctraining.github.io/Intro-to-ChIPseq/lessons/05_peak_calling_macs.html>`_.
For more information about Model-based Analysis of ChIP-Seq (MACS) please see published
`article <https://genomebiology.biomedcentral.com/articles/10.1186/gb-2008-9-9-r137>`_.
