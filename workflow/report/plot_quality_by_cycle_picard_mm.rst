`Mean quality by cycle plot
<https://gatk.broadinstitute.org/hc/en-us/articles/360040506831-MeanQualityByCycle-Picard->`_ **(Picard)** is used as
quality control for sequencing machine performance and collects the mean quality by cycle of the bam files after
filtering, sorting, merging and removing orphans. Clear drops in quality within reads may indicate technical problems
during sequencing. For more information about `collected Picard metrics
<https://gatk.broadinstitute.org/hc/en-us/articles/360037594031-CollectMultipleMetrics-Picard->`_ please
see `documentation <https://broadinstitute.github.io/picard/>`_.
